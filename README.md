# Girls Who Code @ Spreetail

Welcome to Spreetail!

Spreetail is a rapidly growing ecommerce company that got its start in 2006. As a company, we build our own technology, market the products we sell, run warehouses, recruit top-notch talent, and create original community-focused programs that make a real-world impact. We sell thousands of products through numerous online channels, including our own Spreetail.com, and ship them to customers in 1-2 days free of charge. We know that we collaborate and think bigger together, which is why we've spent years building best-inclass in-house teams that are ready to tackle challenges others call impossible.

## Your mission

Today you will be working with product managers, product designers, front end engineers, and back end engineers to create your own feature - a Top 10 List of Spreetail's least and most expensive products.

## Get Started

- run `npm i`
- run `npm start`
- Your app is now running on `localhost:3000`

### Start `girls-who-code-service`

- Follow the instructions in [this repo](https://bitbucket.org/spreetail/girls-who-code-service/src/master/) to get the service up and running.
- Your data should be visible at `http://localhost:8080/swagger`
- Press Ctrl + C in the terminal to quit

### Get data

- Check out branch `step1-data`. This includes a [React hook](https://reactjs.org/docs/hooks-intro.html) called `useEffect` that we will use with axios to fetch our data we created on the backend.

### Create components

- For brevity, I've created all of the components you need in the branch `step2-components`. Feel free to use these, or pick your favorite component library. I've listed a few in the resources section for inspiration.
- Components for this demo were created following the Figma design file we received from the product designers - but feel to add your own spin!

### Pass data through to props and wire up logic

- Using the data we logged in step 1, let's pass data through to each of the components using [props](https://reactjs.org/docs/components-and-props.html).
- Check out the `step3-props` if you need a hint
- Wire up

### The End

And you're all done! You can find the finished code in the `step4-finished` branch. Feel free to play with this repo as much as you want and don't hesistate to ask me any questions: `ashley.woodall@spreetail.com`.

## Resources:

- [React Docs](https://reactjs.org/docs/getting-started.html) - this is the framework we used
- [Styled Components](https://www.styled-components.com/) - this css framework we used to style react components
- [Material UI](https://material-ui.com/) - an alternative you can use to our design system components
- [Bootstrap](https://getbootstrap.com/) - another alternative you can use to our design system components
- [MDN webdocs](https://developer.mozilla.org/en-US/) - THE javascript resource
