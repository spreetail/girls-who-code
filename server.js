const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
const port = 8080;

app.use(express.static(path.join(__dirname, "build")));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(port, () =>
  console.log(`App listening on port ${port}! Press “Ctrl + C” to quit.`)
);
